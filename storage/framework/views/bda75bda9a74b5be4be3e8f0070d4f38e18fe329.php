<?php $__env->startSection('title','User Profiles'); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="row">
            <div class="col-sm-4 pull-right">
                <?php echo e(Form::open(['route'=>['admin.profiles'],'method' => 'get'])); ?>

                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search Name or User ID" name="search"
                           value="<?php echo e(app('request')->input('search')); ?>">
                      <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Go!</button>
                      </span>
                </div>
                <?php echo e(Form::close()); ?>

            </div>
        </div>
        <table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
               width="100%">
            <thead>
            <tr>
                <th><?php echo \Kyslik\ColumnSortable\SortableLink::render(array ('name', 'Name', ['page' => $profiles->currentPage()]));?></th>
		<th>Email</th>
                <th>Gender</th>
                <th>User ID(Firebase)</th>
                <th>Created At</th>
                <th>Update At</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            <?php $__currentLoopData = $profiles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $profile): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td><?php echo e($profile->name); ?></td>
		    <td><?php echo e($profile->email); ?></td>
                    <td><?php echo e($profile->gender); ?></td>
                    <td><?php echo e($profile->user_id); ?></td>
                    <td><?php echo e($profile->created_at); ?></td>
                    <td><?php echo e($profile->updated_at); ?></td>
                    <td>
                        <a class="btn btn-xs btn-primary" href="<?php echo e(route('admin.profiles.show', [$profile->id])); ?>"
                           data-toggle="tooltip" data-placement="top" data-title="View">
                            <i class="fa fa-eye"></i>
                        </a>
                        <a class="btn btn-xs btn-info" href="<?php echo e(route('admin.profiles.edit', [$profile->id])); ?>"
                           data-toggle="tooltip" data-placement="top" data-title="Edit">
                            <i class="fa fa-pencil"></i>
                        </a>
                        <a class="btn btn-xs btn-danger" href="<?php echo e(route('admin.profiles.destroy', [$profile->id])); ?>"
                           data-toggle="tooltip" data-placement="top" data-title="Delete">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
        <div class="pull-right">
            <?php echo e($profiles->links()); ?>

        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
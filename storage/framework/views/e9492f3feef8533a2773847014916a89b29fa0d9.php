<?php $__env->startSection('title', $agama->agama); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <table class="table table-striped table-hover">
            <tbody>
            <tr>
                <th>Agama</th>
                <td><?php echo e($agama->agama); ?></td>
            </tr>

            <tr>
                <th>Created At</th>
                <td><?php echo e($agama->created_at); ?> (<?php echo e($agama->created_at->diffForHumans()); ?>)</td>
            </tr>

            <tr>
                <th>Updated At</th>
                <td><?php echo e($agama->updated_at); ?> <?php echo e((!empty($agama->updated_at)) ? ($agama->updated_at->diffForHumans()) : ''); ?></td>
            </tr>
            </tbody>
        </table>
    </div>
    <a class="btn btn-primary" href="<?php echo e(URL::previous()); ?>">Back</a>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
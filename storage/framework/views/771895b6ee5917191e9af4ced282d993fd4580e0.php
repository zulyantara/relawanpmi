<?php $__env->startSection('title', 'Post'); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <table class="table table-striped table-hover">
            <tbody>
            <tr>
                <th>ID</th>
                <td><?php echo e($post->id); ?></td>
            </tr>

            <tr>
                <th>Type</th>
                <td><?php echo e($post->type); ?></td>
            </tr>

            <tr>
                <th>User</th>
                <td><a href="<?php echo e(route('admin.posts.show', [$post->user_profile_id->id]  )); ?>"><?php echo e($post->user_profile_id->name); ?></a></td>
            </tr>

            <tr>
                <th>Text</th>
                <td><?php echo e($post->text); ?></td>
            </tr>

            <tr>
                <th>Media Url</th>
                <td><a target="_blank" href="<?php echo e($post->media_url); ?>"><?php echo e($post->media_url); ?></a></td>
            </tr>

            <tr>
                <th>Like Count</th>
                <td><?php echo e($post->like_count); ?></td>
            </tr>

            <tr>
                <th>Dislike Count</th>
                <td><?php echo e($post->dislike_count); ?></td>
            </tr>

            <tr>
                <th>Comment Count</th>
                <td><?php echo e($post->comment_count); ?></td>
            </tr>

            <tr>
                <th>Created At</th>
                <td><?php echo e($post->created_at); ?> (<?php echo e($post->created_at->diffForHumans()); ?>)</td>
            </tr>

            <tr>
                <th>Updated At</th>
                <td><?php echo e($post->updated_at); ?> (<?php echo e($post->updated_at->diffForHumans()); ?>)</td>
            </tr>
            </tbody>
        </table>
    </div>
    <a class="btn btn-danger" href="<?php echo e(route('admin.posts.destroy', [$post->id])); ?>"
       data-toggle="tooltip" data-placement="top" data-title="Delete">Delete</a>
    <a class="btn btn-success" href="<?php echo e(route('admin.postactivities', ["post_id" => $post->id])); ?>"
       data-toggle="tooltip" data-placement="top" data-title="Delete">View Activities</a>
    <a class="btn btn-success" href="<?php echo e(route('admin.comments', ["post_id" => $post->id])); ?>"
       data-toggle="tooltip" data-placement="top" data-title="Delete">View Comments</a>
    <a class="btn btn-primary" href="<?php echo e(URL::previous()); ?>">Back</a>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
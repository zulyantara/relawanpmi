<?php $__env->startSection('title', $profile->name . '\'s Profile'); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <table class="table table-striped table-hover">
            <tbody>
            <tr>
                <th>Name</th>
                <td><?php echo e($profile->name); ?></td>
            </tr>

	    <tr>
                <th>Email</th>
                <td><?php echo e($profile->email); ?></td>
            </tr>

            <tr>
                <th>Gender</th>
                <td><?php echo e($profile->gender); ?></td>
            </tr>

            <tr>
                <th>User ID(Firebase)</th>
                <td><?php echo e($profile->user_id); ?></td>
            </tr>

            <tr>
                <th>Created At</th>
                <td><?php echo e($profile->created_at); ?> (<?php echo e($profile->created_at->diffForHumans()); ?>)</td>
            </tr>

            <tr>
                <th>Updated At</th>
                <td><?php echo e($profile->updated_at); ?> (<?php echo e($profile->updated_at->diffForHumans()); ?>)</td>
            </tr>
            </tbody>
        </table>
    </div>
    <a class="btn btn-success" href="<?php echo e(route('admin.posts', ['user_profile_id' => $profile->id])); ?>"
       data-toggle="tooltip" data-placement="top" data-title="Delete">View Posts</a>
    <a class="btn btn-primary" href="<?php echo e(URL::previous()); ?>">Back</a>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
@extends('admin.layouts.admin')

@section('title','Agama')

@section('content')
    <div class="row">
        <div class="row">
            <div class="col-sm-4 pull-right">
                {{ Form::open(['route'=>['admin.agamas'],'method' => 'get']) }}
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search Agama" name="search"
                           value="{{ app('request')->input('search') }}">
                      <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Go!</button>
                      </span>
                </div>
                {{ Form::close() }}
            </div>
        </div>
        <table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
               width="100%">
            <thead>
            <tr>
                <th>@sortablelink('name', 'Name', ['page' => $agamas->currentPage()])</th>
                <th>Created At</th>
                <th>Update At</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($agamas as $agama)
                <tr>
                    <td>{{ $agama->agama }}</td>
                    <td>{{ $agama->created_at }}</td>
                    <td>{{ $agama->updated_at }}</td>
                    <td>
                        <a class="btn btn-xs btn-primary" href="{{ route('admin.agamas.show', [$agama->id]) }}"
                           data-toggle="tooltip" data-placement="top" data-title="View">
                            <i class="fa fa-eye"></i>
                        </a>
                        <a class="btn btn-xs btn-info" href="{{ route('admin.agamas.edit', [$agama->id]) }}"
                           data-toggle="tooltip" data-placement="top" data-title="Edit">
                            <i class="fa fa-pencil"></i>
                        </a>
                        <a class="btn btn-xs btn-danger" href="{{ route('admin.agamas.destroy', [$agama->id]) }}"
                           data-toggle="tooltip" data-placement="top" data-title="Delete">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="pull-right">
            {{ $agamas->links() }}
        </div>
    </div>
@endsection

@extends('admin.layouts.admin')

@section('title', $agama->agama)

@section('content')
    <div class="row">
        <table class="table table-striped table-hover">
            <tbody>
            <tr>
                <th>Agama</th>
                <td>{{ $agama->agama }}</td>
            </tr>

            <tr>
                <th>Created At</th>
                <td>{{ $agama->created_at }} ({{ $agama->created_at->diffForHumans() }})</td>
            </tr>

            <tr>
                <th>Updated At</th>
                <td>{{ $agama->updated_at }} {{ (!empty($agama->updated_at)) ? ($agama->updated_at->diffForHumans()) : '' }}</td>
            </tr>
            </tbody>
        </table>
    </div>
    <a class="btn btn-primary" href="{{ URL::previous() }}">Back</a>
@endsection
